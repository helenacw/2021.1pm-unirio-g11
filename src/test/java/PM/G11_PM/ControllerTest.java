package PM.G11_PM;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.UUID;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import PM.G11_PM.CONTROLLERS.ControllerCiclista;
import PM.G11_PM.CONTROLLERS.ControllerFuncionario;
import PM.G11_PM.MODELS.Aluguel;
import PM.G11_PM.MODELS.Ciclista;
import PM.G11_PM.MODELS.Devolucao;
import PM.G11_PM.MODELS.Funcionario;
import PM.G11_PM.MODELS.MeioDePagamento;
import PM.G11_PM.UTIL.JavalinApp;
import kong.unirest.HttpResponse;
import kong.unirest.JsonNode;
import kong.unirest.Unirest;

class ControllerTest {

	private static JavalinApp app = new JavalinApp(); // inject any dependencies you might have

	@BeforeAll
	static void init() {
		app.start(7010);
	}

	@AfterAll
	static void afterAll() {
		app.stop();
	}

	// ********************************* TESTES CICLISTA
	// *********************************
	@Test
	void postCiclista200() {
		MeioDePagamento meio = new MeioDePagamento("Helena", "12345", "01-01-2030", "123");
		Ciclista ciclista = new Ciclista();

		ciclista.setNome("Helena");
		ciclista.setNascimento("04-03-1998");
		ciclista.setCpf("11999988895");
		ciclista.setPassaporte(new Ciclista.Passaporte());
		ciclista.getPassaporte().setNumero("747854");
		ciclista.getPassaporte().setPais("Brasil");
		ciclista.getPassaporte().setValidade("12-02-2023");
		ciclista.setNacionalidade("brasileira");
		ciclista.setEmail("helen@hotmail.com");
		ciclista.setSenha("helena123456");
		ciclista.setMeioDePagamento(meio);

		HttpResponse<JsonNode> response = Unirest.post("http://localhost:7010/ciclista").body(ciclista).asJson();
		assertEquals(201, response.getStatus());
	}

	@Test
	void postCiclista405() {
		MeioDePagamento meio = new MeioDePagamento("000", "12345", "01-01-2030", "123");
		Ciclista ciclista = new Ciclista();

		ciclista.setNome("Luiza");
		ciclista.setNascimento("04-07-2008");
		ciclista.setCpf("2026055521");
		ciclista.setPassaporte(new Ciclista.Passaporte());
		ciclista.getPassaporte().setNumero("7410000");
		ciclista.getPassaporte().setPais("Brasil");
		ciclista.getPassaporte().setValidade("12-09-2026");
		ciclista.setNacionalidade("brasileira");
		ciclista.setEmail("x");
		ciclista.setSenha("senha1234967");
		ciclista.setMeioDePagamento(meio);

		HttpResponse<JsonNode> response = Unirest.post("http://localhost:7010/ciclista").body(ciclista).asJson();
		assertEquals(405, response.getStatus());
	}

	@Test
	void getCiclistaById200() {
		String id = (ControllerCiclista.mockGetCiclista().getId());
		HttpResponse<JsonNode> response = Unirest.get("http://localhost:7010/ciclista/" + id).asJson();

		assertEquals(200, response.getStatus());
	}

	@Test
	void getCiclistaById404() {
		String id = (ControllerCiclista.mockGetCiclista404().getId());
		HttpResponse<JsonNode> response = Unirest.get("http://localhost:7010/ciclista/" + id).asJson();

		assertEquals(404, response.getStatus());
	}

	@Test
	void getCiclistaById422() {
		String id = (ControllerCiclista.mockGetCiclista422().getId());
		HttpResponse<JsonNode> response = Unirest.get("http://localhost:7010/ciclista/" + id).asJson();

		assertEquals(422, response.getStatus());
	}

	@Test
	void putCiclistaById200() {
		Ciclista test = ControllerCiclista.mockGetCiclista();
		HttpResponse<String> response = Unirest.put("http://localhost:7010/ciclista/" + test.getId())
				.body("{\"nome\":\"joao\"}").asString();
		assertEquals(200, response.getStatus());
	}

	@Test
	void putCiclistaById404() {
		Ciclista test = ControllerCiclista.mockGetCiclista404();
		HttpResponse<String> response = Unirest.put("http://localhost:7010/ciclista/" + test.getId())
				.body("{\"nome\":\"Luiza\"}").asString();
		assertEquals(404, response.getStatus());
	}

	@Test 
	void putCiclistaById405() {
		Ciclista test = ControllerCiclista.mockGetCiclista405();
		HttpResponse<String> response = Unirest.put("http://localhost:7010/ciclista/" + test.getId())
				.body("{\"nome\":\"Gi\"}").asString();
		assertEquals(405, response.getStatus());
	}

	@Test
	void getEmail200() {
		String email = (ControllerCiclista.mockGetEmail().getEmail());
		HttpResponse<JsonNode> response = Unirest.get("http://localhost:7010/ciclista/existeEmail/" + email).asJson();

		assertEquals(200, response.getStatus());
	}

	@Test
	void getEmail400() {
		String email = ControllerCiclista.mockGetEmail400().getEmail().toString();
		HttpResponse<JsonNode> response = Unirest.get("http://localhost:7010/ciclista/existeEmail/" + email).asJson();

		assertEquals(400, response.getStatus());
	}

	@Test
	void getEmail422() {
		String email = "bla";
		HttpResponse<JsonNode> response = Unirest.get("http://localhost:7010/ciclista/existeEmail/" + email).asJson();

		assertEquals(422, response.getStatus());
	}

	// ********************************* TESTES FUNCIONARIO
	// *********************************

	@Test
	void getFuncionario200() {
		HttpResponse<JsonNode> response = Unirest.get("http://localhost:7010/funcionario/").asJson();

		assertEquals(200, response.getStatus());
	}

	@Test
	void postFuncionario200() {
		Funcionario funcionario = new Funcionario("12345", "senha", "email@email.com", "Jorge", 18, "Gerente",
				"12457878787");

		HttpResponse<JsonNode> response = Unirest.post("http://localhost:7010/funcionario").body(funcionario).asJson();
		assertEquals(200, response.getStatus());
	}

	@Test
	void postFuncionario422() {
		Funcionario funcionario = new Funcionario("8989", "senha", "mail", "Marcela", 32, "Ajudante", "12457878787");

		HttpResponse<JsonNode> response = Unirest.post("http://localhost:7010/funcionario").body(funcionario).asJson();
		assertEquals(422, response.getStatus());
	}

	@Test
	void getFuncionarioById200() {
		String id = (ControllerFuncionario.mockGetFuncionario().getMatricula());
		HttpResponse<JsonNode> response = Unirest.get("http://localhost:7010/funcionario/" + id).asJson();

		assertEquals(200, response.getStatus());
	}

	@Test
	void getFuncionarioById404() {
		String id = (ControllerFuncionario.mockGetFuncionario404().getMatricula());
		HttpResponse<JsonNode> response = Unirest.get("http://localhost:7010/funcionario/" + id).asJson();

		assertEquals(404, response.getStatus());
	}

	@Test
	void getFuncionarioById422() {
		String id = "aaa";
		HttpResponse<JsonNode> response = Unirest.get("http://localhost:7010/funcionario/" + id).asJson();

		assertEquals(422, response.getStatus());
	}

	@Test
	void putFuncionarioById200() {
		Funcionario test = ControllerFuncionario.mockGetFuncionario();
		HttpResponse<String> response = Unirest.put("http://localhost:7010/funcionario/" + test.getMatricula())
				.body("{\"nome\":\"Olga\"}").asString();
		assertEquals(200, response.getStatus());
	}

	@Test // ********************** erro
	void putFuncionarioById404() {
		Funcionario test = ControllerFuncionario.mockGetFuncionario404();
		HttpResponse<String> response = Unirest.put("http://localhost:7010/funcionario/" + test.getMatricula())
				.body("{\"nome\":\"Jessica\"}").asString();
		assertEquals(404, response.getStatus());
	}

	@Test
	void putFuncionarioById422() {
		Funcionario test = ControllerFuncionario.mockGetFuncionario422();
		HttpResponse<String> response = Unirest.put("http://localhost:7010/funcionario/" + test.getMatricula())
				.body("{\"nome\":12}").asString();
		assertEquals(422, response.getStatus());
	}

	@Test
	void deleteFuncionario200() {
		Funcionario test = ControllerFuncionario.mockGetFuncionario();
		HttpResponse<String> response = Unirest.delete("http://localhost:7010/funcionario/" + test.getMatricula())
				.asString();
		assertEquals(200, response.getStatus());
	}

	@Test // ************** erro
	void deleteFuncionario404() {
		HttpResponse<String> response = Unirest.delete("http://localhost:7010/funcionario/" + UUID.randomUUID())
				.asString();
		assertEquals(404, response.getStatus());
	}

	@Test
	void deleteFuncionario422() {
		String test = "qqqq";
		HttpResponse<String> response = Unirest.delete("http://localhost:7010/funcionario/" + test).asString();
		assertEquals(422, response.getStatus());
	}

	// ********************************* TESTES CARTAO DE CREDITO
	// *********************************

	@Test
	void getCartaoById200() {
		String id = UUID.randomUUID().toString();
		HttpResponse<JsonNode> response = Unirest.get("http://localhost:7010/cartaoDeCredito/" + id).asJson();

		assertEquals(200, response.getStatus());
	}

	@Test
	void getCartaoById404() {
		String id = UUID.randomUUID().toString();
		HttpResponse<JsonNode> response = Unirest.get("http://localhost:7010/cartaoDeCredito/" + id).asJson();

		assertEquals(404, response.getStatus());
	}

	@Test
	void getCartaoById422() {
		String id = "7899999";
		HttpResponse<JsonNode> response = Unirest.get("http://localhost:7010/cartaoDeCredito/" + id).asJson();

		assertEquals(422, response.getStatus());
	}

	@Test
	void putCartaoById200() {
		HttpResponse<String> response = Unirest
				.put("http://localhost:7010/cartaoDeCredito/" + UUID.randomUUID().toString())
				.body("{\"numero\":\"1234567890\"}").asString();
		assertEquals(200, response.getStatus());
	}

	@Test
	void putCartaoById404() {
		HttpResponse<String> response = Unirest
				.put("http://localhost:7010/cartaoDeCredito/" + UUID.randomUUID().toString())
				.body("{\"numero\":\"987456321\"}").asString();
		assertEquals(404, response.getStatus());
	}

	@Test
	void putCartaoById422() {
		String test = "eee";
		HttpResponse<String> response = Unirest.put("http://localhost:7010/cartaoDeCredito/" + test)
				.body("{\"numero\":\"12347777\"}").asString();
		assertEquals(422, response.getStatus());
	}

	// ********************************* TESTES ALUGUEL
	// *********************************

	@Test
	void postAluguel200() {
		Aluguel aluguel = new Aluguel(UUID.randomUUID().toString(), UUID.randomUUID().toString());

		HttpResponse<JsonNode> response = Unirest.post("http://localhost:7010/aluguel").body(aluguel).asJson();
		assertEquals(200, response.getStatus());
	}

	@Test
	void postAluguel422() {
		Aluguel aluguel = new Aluguel("eee", UUID.randomUUID().toString());

		HttpResponse<JsonNode> response = Unirest.post("http://localhost:7010/aluguel").body(aluguel).asJson();
		assertEquals(422, response.getStatus());
	}

	// ********************************* TESTES DEVOLUCAO
	// *********************************

	@Test
	void postDevolucao200() {
		Devolucao devolucao = new Devolucao(UUID.randomUUID().toString(), UUID.randomUUID().toString());

		HttpResponse<JsonNode> response = Unirest.post("http://localhost:7010/devolucao").body(devolucao).asJson();
		assertEquals(200, response.getStatus());
	}

	@Test
	void postDevolucao422() {
		Devolucao devolucao = new Devolucao(UUID.randomUUID().toString(), ".");

		HttpResponse<JsonNode> response = Unirest.post("http://localhost:7010/devolucao").body(devolucao).asJson();
		assertEquals(422, response.getStatus());
	}

}
