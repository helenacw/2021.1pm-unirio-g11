package PM.G11_PM.MODELS;

public class Aluguel {
	private String ciclista;
	private String trancaInicio;

	public Aluguel(String ciclista, String trancaInicio) {
		super();
		this.ciclista = ciclista;
		this.trancaInicio = trancaInicio;
	}

	public String getCiclista() {
		return ciclista;
	}

	public void setCiclista(String ciclista) {
		this.ciclista = ciclista;
	}

	public String getTrancaInicio() {
		return trancaInicio;
	}

	public void setTrancaInicio(String trancaInicio) {
		this.trancaInicio = trancaInicio;
	}

}
