package PM.G11_PM.MODELS;

public class Devolucao {
	private String idTranca;
	private String idBicicleta;

	public Devolucao(String idTranca, String idBicicleta) {
		super();
		this.idTranca = idTranca;
		this.idBicicleta = idBicicleta;
	}

	public String getIdTranca() {
		return idTranca;
	}

	public void setIdTranca(String idTranca) {
		this.idTranca = idTranca;
	}

	public String getIdBicicleta() {
		return idBicicleta;
	}

	public void setIdBicicleta(String idBicicleta) {
		this.idBicicleta = idBicicleta;
	}

}
