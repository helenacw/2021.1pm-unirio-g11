package PM.G11_PM.SERVICES;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import com.google.gson.Gson;

import PM.G11_PM.MODELS.Aluguel;

public class AluguelService {
	private static String regexUuid = "[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}";
	private static Pattern p = Pattern.compile(regexUuid);
	static List<Aluguel> alugueis = new ArrayList<Aluguel>();

	private AluguelService() {
	}

	public static Aluguel postAluguel(String body) {
		Gson gson = new Gson();
		Aluguel aluguel = gson.fromJson(body, Aluguel.class);

		if (p.matcher(aluguel.getCiclista()).matches()) {
			Aluguel aluguelNovo = new Aluguel(aluguel.getCiclista(), aluguel.getTrancaInicio());

			alugueis.add(aluguelNovo);
			System.out.println(aluguelNovo);
			return aluguelNovo;
		} else
			return null;

	}
}
