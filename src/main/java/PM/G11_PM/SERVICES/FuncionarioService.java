package PM.G11_PM.SERVICES;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import com.google.gson.Gson;

import PM.G11_PM.MODELS.Funcionario;

public class FuncionarioService {
	public static List<Funcionario> funcionarios = new ArrayList<Funcionario>();
	private static String regexUuid = "[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}";
	private static Pattern p = Pattern.compile(regexUuid);
	public static final Pattern email = Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$",
			Pattern.CASE_INSENSITIVE);

	private FuncionarioService() {
	}

	public static String getFuncionario() {
		return funcionarios.toString();
	}

	public static void envia(Funcionario funcionario) {
		funcionarios.add(funcionario);
	}

	public static Funcionario postFuncionario(String body) {
		Gson gson = new Gson();
		Funcionario funcionario = gson.fromJson(body, Funcionario.class);

		if (email.matcher(funcionario.getEmail()).matches()) {
			Funcionario funcionarioNovo = new Funcionario(funcionario.getMatricula(), funcionario.getSenha(),
					funcionario.getEmail(), funcionario.getNome(), funcionario.getIdade(), funcionario.getFuncao(),
					funcionario.getCpf());

			funcionarios.add(funcionarioNovo);
			return funcionarioNovo;
		} else
			return null;

	}

	public static Funcionario getById(String id) {
		Funcionario temp = null;
		if (p.matcher(id).matches() == true) {
			temp = funcionarios.stream().filter(funcionarios -> id.equals(funcionarios.getMatricula())).findAny()
					.orElse(null);
		}
		return temp;
	}

	public static Funcionario putFuncionarioById(String id, String body) {
		Funcionario temp = null;
		if (p.matcher(id).matches() == true) {
			temp = funcionarios.stream().filter(funcionarios -> id.equals(funcionarios.getMatricula())).findAny()
					.orElse(null);

			if (temp != null) {
				Gson gson = new Gson();
				Funcionario funcionarioNovo = gson.fromJson(body, Funcionario.class);

				if (funcionarioNovo.getSenha() != null)
					temp.setSenha(funcionarioNovo.getSenha());
				if (funcionarioNovo.getEmail() != null)
					temp.setEmail(funcionarioNovo.getEmail());
				if (funcionarioNovo.getNome() != null)
					temp.setNome(funcionarioNovo.getNome());

				return temp;
			}
			return null;
		}

		return temp;
	}

	public static void deleteFuncionarioById(String id) {
		funcionarios.removeIf(funcionario -> funcionario.getMatricula().equals(id));

	}

}
