package PM.G11_PM.SERVICES;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import com.google.gson.Gson;

import PM.G11_PM.MODELS.Devolucao;

public class DevolucaoService {
	private static String regexUuid = "[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}";
	private static Pattern p = Pattern.compile(regexUuid);
	public static List<Devolucao> devolucoes = new ArrayList<Devolucao>();

	private DevolucaoService() {
	}

	public static void envia(Devolucao devolucao) {
		devolucoes.add(devolucao);
	}

	public static Devolucao postDevolucao(String body) {
		Gson gson = new Gson();
		Devolucao devolucao = gson.fromJson(body, Devolucao.class);

		if (p.matcher(devolucao.getIdBicicleta()).matches()) {
			Devolucao devolucaoNovo = new Devolucao(devolucao.getIdTranca(), devolucao.getIdBicicleta());

			devolucoes.add(devolucaoNovo);
			return devolucaoNovo;
		}

		else
			return null;

	}
}
