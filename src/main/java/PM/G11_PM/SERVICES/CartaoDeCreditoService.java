package PM.G11_PM.SERVICES;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import com.google.gson.Gson;

import PM.G11_PM.MODELS.Ciclista;
import PM.G11_PM.MODELS.MeioDePagamento;

public class CartaoDeCreditoService {
	public static List<MeioDePagamento> cartoes = new ArrayList<MeioDePagamento>();
	private static String regexUuid = "[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}";
	private static Pattern p = Pattern.compile(regexUuid);

	private CartaoDeCreditoService() {
	}

	public static void envia(MeioDePagamento cartao) {
		cartoes.add(cartao);
	}

	public static MeioDePagamento putCartaoById(Ciclista ciclista, String body) {
		if (p.matcher(ciclista.getId()).matches() == true) {
			MeioDePagamento temp = ciclista.getMeioDePagamento();
			if (temp != null) {
				Gson gson = new Gson();
				MeioDePagamento cartaoNovo = gson.fromJson(body, MeioDePagamento.class);

				if (cartaoNovo.getNomeTitular() != null)
					temp.setNomeTitular(cartaoNovo.getNomeTitular());
				if (cartaoNovo.getNumero() != null)
					temp.setNumero(cartaoNovo.getNumero());
				if (cartaoNovo.getValidade() != null)
					temp.setValidade(cartaoNovo.getValidade());
				if (cartaoNovo.getCvv() != null)
					temp.setCvv(cartaoNovo.getCvv());
				return temp;
			}
			return null;

		}
		return null;

	}
}
