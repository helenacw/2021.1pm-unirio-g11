package PM.G11_PM.SERVICES;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import com.google.gson.Gson;

import PM.G11_PM.MODELS.Ciclista;

public class CiclistaService {
	public static List<Ciclista> ciclistas = new ArrayList<Ciclista>();
	// regex pra checar a estrutura do uuid gerado
	private static String regexUuid = "[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}";
	private static Pattern p = Pattern.compile(regexUuid);
	public static final Pattern email = Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$",
			Pattern.CASE_INSENSITIVE);

	private CiclistaService() {
	}

	public static void envia(Ciclista ciclista) {
		ciclistas.add(ciclista);
	}

	public static String getCiclista() {
		return ciclistas.toString();
	}

	public static Ciclista postCiclista(String body) {
		Gson gson = new Gson();
		Ciclista teste = gson.fromJson(body, Ciclista.class);
		System.out.println(teste);

		if (email.matcher(teste.getEmail()).matches()) {
			Ciclista novo = new Ciclista();
			novo.setNome(teste.getNome());
			novo.setNascimento(teste.getNascimento());
			novo.setCpf(teste.getCpf());
			novo.setPassaporte(teste.getPassaporte());
			novo.setNacionalidade(teste.getNacionalidade());
			novo.setEmail(teste.getEmail());
			novo.setSenha(teste.getSenha());
			novo.setMeioDePagamento(teste.getMeioDePagamento());
			
			System.out.println(teste.getMeioDePagamento().toString());

			ciclistas.add(novo);
			return novo;
		} else
			return null;

	}
	
	public static Ciclista getById(String id) {
		Ciclista temp = null;
		if (p.matcher(id).matches() == true) {
			temp = ciclistas.stream().filter(ciclista -> id.equals(ciclista.getId())).findAny().orElse(null);
		}
		return temp;

	}

	public static Ciclista putCiclistaById(String id, String body) {
		Ciclista temp = null;
		if (p.matcher(id).matches() == true) {
			temp = ciclistas.stream().filter(ciclista -> id.equals(ciclista.getId())).findAny().orElse(null);

			if (temp != null) {
				Gson gson = new Gson();
				Ciclista ciclistaNovo = gson.fromJson(body, Ciclista.class);

				if (ciclistaNovo.getCpf() != null)
					temp.setCpf(ciclistaNovo.getCpf());
				if (ciclistaNovo.getNome() != null)
					temp.setNome(ciclistaNovo.getNome());
				if (ciclistaNovo.getNascimento() != null)
					temp.setNascimento(ciclistaNovo.getNascimento());

				if (ciclistaNovo.getPassaporte() != null) {
					temp.setPassaporte(ciclistaNovo.getPassaporte());
				}

				if (ciclistaNovo.getNacionalidade() != null)
					temp.setNacionalidade(ciclistaNovo.getNacionalidade());
				if (ciclistaNovo.getEmail() != null)
					temp.setEmail(ciclistaNovo.getEmail());
				return temp;
			}
			return null;
		}
		return temp;
	}

	public static Ciclista postCiclistaAtivar(String id, String body) {
		Gson gson = new Gson();
		Ciclista teste = gson.fromJson(body, Ciclista.class);

		if (p.matcher(id).matches() == true) {
			Ciclista novo = new Ciclista();
			novo.setNome(teste.getNome());
			novo.setNascimento(teste.getNascimento());
			novo.setCpf(teste.getCpf());
			novo.setPassaporte(teste.getPassaporte());
			novo.setNacionalidade(teste.getNacionalidade());
			novo.setEmail(teste.getEmail());
			novo.setSenha(teste.getSenha());
			novo.setMeioDePagamento(teste.getMeioDePagamento());

			ciclistas.add(novo);
			return novo;
		} else
			return null;
	}

	public static Ciclista getCiclistaEmail(String mail) {
		Ciclista temp = null;
		if (email.matcher(mail).matches()) {
			temp = ciclistas.stream().filter(ciclista -> mail.equals(ciclista.getEmail())).findAny().orElse(null);
		}
		return temp;
	}

}
