package PM.G11_PM.UTIL;

import static io.javalin.apibuilder.ApiBuilder.delete;
import static io.javalin.apibuilder.ApiBuilder.get;
import static io.javalin.apibuilder.ApiBuilder.path;
import static io.javalin.apibuilder.ApiBuilder.post;
import static io.javalin.apibuilder.ApiBuilder.put;

import PM.G11_PM.CONTROLLERS.ControllerAluguel;
import PM.G11_PM.CONTROLLERS.ControllerCartaoDeCredito;
import PM.G11_PM.CONTROLLERS.ControllerCiclista;
import PM.G11_PM.CONTROLLERS.ControllerDevolucao;
import PM.G11_PM.CONTROLLERS.ControllerFuncionario;
import io.javalin.Javalin;

public class JavalinApp {

	private static final String CICLISTA_ID = "/ciclista/:id";
	private static final String FUNCIONARIO_ID = "/funcionario/:id";

	private Javalin app = Javalin.create(config -> config.defaultContentType = "application/json").routes(() -> {
		// caminhos 'ciclista'
		path("/ciclista", () -> post(ControllerCiclista::postCiclista));
		path(CICLISTA_ID, () -> get(ControllerCiclista::getCiclistaById));
		path(CICLISTA_ID, () -> put(ControllerCiclista::putCiclistaById));
		path(CICLISTA_ID + "/ativar", () -> post(ControllerCiclista::postCiclistaAtivar));
		path("/ciclista/existeEmail/:email", () -> get(ControllerCiclista::getCiclistaEmail));

		// caminhos 'funcionario'
		path("/funcionario", () -> get(ControllerFuncionario::getFuncionario));
		path("/funcionario", () -> post(ControllerFuncionario::postFuncionario));
		path(FUNCIONARIO_ID, () -> get(ControllerFuncionario::getFuncionarioById));
		path(FUNCIONARIO_ID, () -> put(ControllerFuncionario::putFuncionarioById));
		path(FUNCIONARIO_ID, () -> delete(ControllerFuncionario::deleteFuncionarioById));

		// caminhos 'cartaoDeCredito'
		path("/cartaoDeCredito/:id", () -> get(ControllerCartaoDeCredito::getCartaoById));
		path("/cartaoDeCredito/:id", () -> put(ControllerCartaoDeCredito::putCartaoById));

		// caminho 'aluguel'
		path("/aluguel", () -> post(ControllerAluguel::postAluguel));

		// caminho 'devolucao'
		path("/devolucao", () -> post(ControllerDevolucao::postDevolucao));

	});

	public void start(int port) {
		this.app.start(port);
	}

	public void stop() {
		this.app.stop();
	}
}
