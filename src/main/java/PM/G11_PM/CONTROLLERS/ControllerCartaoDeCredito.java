package PM.G11_PM.CONTROLLERS;

import static PM.G11_PM.MODELS.Status.status;

import java.util.ArrayList;
import java.util.List;

import PM.G11_PM.MODELS.Ciclista;
import PM.G11_PM.MODELS.MeioDePagamento;
import PM.G11_PM.SERVICES.CartaoDeCreditoService;
import PM.G11_PM.SERVICES.CiclistaService;
import io.javalin.http.Context;

public class ControllerCartaoDeCredito {
	static List<MeioDePagamento> cartoes = new ArrayList<MeioDePagamento>();
	private static String dadosInvalidos = "Dados inválidos";
	private static String dadosCadastrados = "Dados cadastrados";
	private static String dadosNaoEcontrados = "Dados não encontrados";
	
	private ControllerCartaoDeCredito() {}

	public static void getCartaoById(Context ctx) {
		String id = ctx.pathParam("id");
		Ciclista ciclista = CiclistaService.getById(id);

		if (ciclista != null) {
			MeioDePagamento meio = ciclista.getMeioDePagamento();

			if (meio != null) {
				ctx.status(200);
				ctx.result(meio.toString());
			} else {
				ctx.status(404);
				ctx.result(status("404", dadosNaoEcontrados));
			}
		} else {
			ctx.status(422);
			ctx.result(status("422", dadosInvalidos));
		}
	}

	public static void putCartaoById(Context ctx) {
		String body = ctx.body();
		String id = ctx.pathParam("id");
		Ciclista ciclista = CiclistaService.getById(id);

		if (ciclista != null)  {
			CartaoDeCreditoService.putCartaoById(ciclista, body);
			MeioDePagamento meio = ciclista.getMeioDePagamento();

			if (meio != null) {
				ctx.status(200);
				ctx.result(status("200", dadosCadastrados));
			} else {
				ctx.status(404);
				ctx.result(status("404", dadosNaoEcontrados));
			}
		} else {
			ctx.status(422);
			ctx.result(status("422", dadosInvalidos));
		}

	}


}
