package PM.G11_PM.CONTROLLERS;

import static PM.G11_PM.MODELS.Status.status;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.regex.Pattern;

import PM.G11_PM.MODELS.Funcionario;
import PM.G11_PM.SERVICES.FuncionarioService;
import io.javalin.http.Context;

public class ControllerFuncionario {
	private static List<Funcionario> funcionarios = new ArrayList<Funcionario>();
	private static String dadosInvalidos = "Dados inválidos";
	private static String dadosCadastrados = "Dados cadastrados";
	private static String dadosNaoEcontrados = "Dados não encontrados";
	private static String dadosRemovidos = "Dados removidos";
	private static String regexUuid = "[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}";
	private static Pattern p = Pattern.compile(regexUuid);

	public static void getFuncionario(Context ctx) {
		String result = FuncionarioService.getFuncionario();
		ctx.result(result);
		ctx.status(200);
	}

	public static void postFuncionario(Context ctx) {
		String body = ctx.body();
		Funcionario validarResponse = FuncionarioService.postFuncionario(body);
		

		if (validarResponse != null) {
			ctx.status(200);
			funcionarios.add((Funcionario) validarResponse);
			ctx.result(status("200", dadosCadastrados + " - id: " + validarResponse.getId().toString()));
		} else {
			ctx.status(422);
			ctx.result(status("422", dadosInvalidos));
		}

	}

	public static void getFuncionarioById(Context ctx) {
		String id = ctx.pathParam("id");
		Funcionario funcionario = FuncionarioService.getById(id);

		if (funcionario != null) {
			Funcionario temp = funcionarios.stream().filter(f -> id.equals(f.getMatricula())).findAny().orElse(null);

			if (temp != null) {
				ctx.status(200);
				ctx.result(status("200", dadosCadastrados));
			} else {
				ctx.status(404);
				ctx.result(status("404", dadosNaoEcontrados));
			}
		} else {
			ctx.status(422);
			ctx.result(status("422", dadosInvalidos));
		}
	}

	public static void putFuncionarioById(Context ctx) {
		String id = ctx.pathParam("id");
		String body = ctx.body();
		Funcionario funcionario = FuncionarioService.getById(id);

		if (funcionario != null) {
			Funcionario temp = funcionarios.stream().filter(f -> id.equals(f.getMatricula())).findAny().orElse(null);

			if (temp != null) {
				FuncionarioService.putFuncionarioById(id, body);
				ctx.status(200);
				ctx.result(status("200", dadosCadastrados));
			} else {
				ctx.status(404);
				ctx.result(status("404", dadosNaoEcontrados));
			}
		} else {
			ctx.status(422);
			ctx.result(status("422", dadosInvalidos));
		}
	}

	public static void deleteFuncionarioById(Context ctx) {
		String id = ctx.pathParam("id");
		if (p.matcher(id).matches()) {
			Funcionario funcionario = FuncionarioService.getById(id);
			if (funcionario != null) {

				FuncionarioService.deleteFuncionarioById(id);
				ctx.status(200);
				ctx.result(status("200", dadosRemovidos));
			} else {
				ctx.status(404);
				ctx.result(status("404", dadosNaoEcontrados));
			}
		} else {
			ctx.status(422);
			ctx.result(status("422", dadosInvalidos));
		}

	}

	public static Funcionario mockGetFuncionario() {
		FuncionarioService.funcionarios.clear();
		String id = UUID.randomUUID().toString();
		Funcionario funcionario = new Funcionario(id, "senha", "eeee@email.com", "Lucas", 18, "Gerente", "7777777");
		funcionarios.add(funcionario);
		FuncionarioService.envia(funcionario);

		return FuncionarioService.funcionarios.get(FuncionarioService.funcionarios.size() - 1);

	}

	public static Funcionario mockGetFuncionario404() {
		FuncionarioService.funcionarios.clear();
		String id2 = UUID.randomUUID().toString();
		Funcionario funcionario2 = new Funcionario(id2, "senha", "email@email.com", "Maria", 25, "Administradora",
				"12457878787");
		FuncionarioService.envia(funcionario2);

		return FuncionarioService.funcionarios.get(FuncionarioService.funcionarios.size() - 1);

	}

	public static Funcionario mockGetFuncionario422() {
		FuncionarioService.funcionarios.clear();
		String id = "hehe";
		Funcionario funcionario = new Funcionario(id, "senha", "m", "Jorge", 18, "Gerente", "888888888");
		funcionarios.add(funcionario);
		FuncionarioService.envia(funcionario);

		return FuncionarioService.funcionarios.get(FuncionarioService.funcionarios.size() - 1);

	}
	
}
