package PM.G11_PM.CONTROLLERS;

import static PM.G11_PM.CONTROLLERS.ControllerAluguel.enviaEmail;
import static PM.G11_PM.MODELS.Status.status;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import PM.G11_PM.MODELS.Cobranca;
import PM.G11_PM.MODELS.Devolucao;
import PM.G11_PM.SERVICES.CiclistaService;
import PM.G11_PM.SERVICES.DevolucaoService;
import io.javalin.http.Context;
import kong.unirest.HttpResponse;
import kong.unirest.JsonNode;
import kong.unirest.Unirest;

public class ControllerDevolucao {
	static List<Devolucao> devolucoes = new ArrayList<Devolucao>();
	private static String dadosInvalidos = "Dados inválidos";
	private static String dadosCadastrados = "Dados cadastrados";
	
	private ControllerDevolucao() {}

	public static void postDevolucao(Context ctx) {
		String body = ctx.body();
		Devolucao validarResposta = DevolucaoService.postDevolucao(body);

		if (validarResposta != null) {
			ctx.status(200);
			devolucoes.add(validarResposta);
			ctx.result(status("200", dadosCadastrados));
			alterarEstadoTranca("TRANCAR");
			incluirNaFilaCobranca();
			enviaEmail();
		} else {
			ctx.status(422);
			ctx.result(status("422", dadosInvalidos));
		}
	}
	
	public static void alterarEstadoTranca(String acao) {
		Tranca nova = new Tranca(1, "sp", "2010", "a1", "disponível");
		String idTranca = "3fa85f64-5717-4562-b3fc-2c963f66afa6";
		HttpResponse<JsonNode> response = Unirest.post("https://trabalho-pm-g10.herokuapp.com/tranca/" + idTranca + "/status/" + acao).body(nova).asJson();
		if (response.getStatus() == 200) {
			System.out.println("Ação bem sucedida");
		} else {
			System.out.println(dadosInvalidos);
		}
	}

	public static class Tranca {
		int numero;
		String localizacao;
		String anoDeFabricacao;
		String modelo;
		String status;
		
		Tranca () {
			
		}
		
		public Tranca(int numero, String localizacao, String anoDeFabricacao, String modelo, String status) {
			this.numero = numero;
			this.localizacao = localizacao;
			this.anoDeFabricacao = anoDeFabricacao;
			this.modelo = modelo;
			this.status = status;
		}
	}
	
	public static void incluirNaFilaCobranca() {
		String idCiclista = CiclistaService.ciclistas.get(1).toString();
		Cobranca nova = new Cobranca(6.0f, idCiclista);
		HttpResponse<JsonNode> response = Unirest.post("https://pm-g4-brenofil.herokuapp.com/filaCobranca/").body(nova)
				.asJson();
		if (response.getStatus() == 200) {
			System.out.println("Cobrança feita com sucesso");
		} else {
			System.out.println("Erro ao fazer a cobrança");
		}
	}
	
	
	public static Devolucao mockGetDevolucao422() {
		String tranca = UUID.randomUUID().toString();
		Devolucao devolucao = new Devolucao("oi", tranca);
		devolucoes.add(devolucao);
		DevolucaoService.envia(devolucao);

		return DevolucaoService.devolucoes.get(DevolucaoService.devolucoes.size() - 1);

	}
}
