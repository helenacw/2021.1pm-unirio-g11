package PM.G11_PM.CONTROLLERS;

import static PM.G11_PM.CONTROLLERS.ControllerDevolucao.alterarEstadoTranca;
import static PM.G11_PM.MODELS.Status.status;

import java.util.ArrayList;
import java.util.List;

import PM.G11_PM.MODELS.Aluguel;
import PM.G11_PM.MODELS.Ciclista;
import PM.G11_PM.MODELS.Cobranca;
import PM.G11_PM.SERVICES.AluguelService;
import PM.G11_PM.SERVICES.CiclistaService;
import io.javalin.http.Context;
import kong.unirest.HttpResponse;
import kong.unirest.JsonNode;
import kong.unirest.Unirest;

public class ControllerAluguel {
	static List<Aluguel> alugueis = new ArrayList<Aluguel>();
	private static String dadosInvalidos = "Dados inválidos";
	private static String dadosCadastrados = "Dados cadastrados";

	private ControllerAluguel() {
	}

	public static void postAluguel(Context ctx) {
		String body = ctx.body();
		Aluguel validarResposta = AluguelService.postAluguel(body);

		if (validarResposta != null) {
			ctx.status(200);
			alugueis.add(validarResposta);
			ctx.result(status("200", dadosCadastrados));
			realizaCobranca();
			alterarEstadoTranca("DESTRANCAR");
			enviaEmail();

		} else {
			ctx.status(422);
			ctx.result(status("422", dadosInvalidos));
		}

	}

	public static void realizaCobranca() {
		String idCiclista = CiclistaService.ciclistas.get(1).toString();
		Cobranca nova = new Cobranca(3.0f, idCiclista);
		HttpResponse<JsonNode> response = Unirest.post("https://pm-g4-brenofil.herokuapp.com/cobranca/").body(nova)
				.asJson();
		if (response.getStatus() == 200) {
			System.out.println("Cobrança feita com sucesso");
		} else {
			System.out.println("Erro ao fazer a cobrança");
		}
	}

	public static void enviaEmail() {
		String email = CiclistaService.ciclistas.get(1).getEmail();
		Email novo = new Email(email, "Requisição recebida");
		HttpResponse<JsonNode> response = Unirest.post("https://pm-g4-brenofil.herokuapp.com/enviarEmail").body(novo)
				.asJson();
		if (response.getStatus() == 200) {
			System.out.println("Email enviado com sucesso");
		} else {
			System.out.println("Erro ao enviar email");
		}
	}

	public static class Email {
		String email;
		String mensagem;

		Email() {

		}

		public Email(String email, String mensagem) {
			this.email = email;
			this.mensagem = mensagem;
		}
	}

}
