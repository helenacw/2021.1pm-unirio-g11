package PM.G11_PM.CONTROLLERS;

import static PM.G11_PM.MODELS.Status.status;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.regex.Pattern;

import com.fasterxml.jackson.annotation.JsonProperty;

import PM.G11_PM.MODELS.Ciclista;
import PM.G11_PM.MODELS.Ciclista.Passaporte;
import PM.G11_PM.MODELS.MeioDePagamento;
import PM.G11_PM.SERVICES.CiclistaService;
import io.javalin.http.Context;

public class ControllerCiclista {

	private static List<Ciclista> ciclistas = new ArrayList<Ciclista>();
	private static String dadosInvalidos = "Dados inválidos";
	private static String dadosCadastrados = "Dados cadastrados";
	private static String dadosNaoEcontrados = "Dados não encontrados";
	public static final Pattern email = Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$",
			Pattern.CASE_INSENSITIVE);

	private ControllerCiclista() {
	}


	public static void postCiclista(Context ctx) {
		PostCiclista cadastrarCiclistaDTO = ctx
				.bodyAsClass(PostCiclista.class);

		Ciclista obterCiclista = cadastrarCiclistaDTO.ciclista.obterCiclista();
		System.out.println(obterCiclista);

		ciclistas.add(obterCiclista);
		obterCiclista.setId(UUID.randomUUID().toString());
		ctx.json(new CiclistaNovo(obterCiclista));

	}

	public static void getCiclistaById(Context ctx) {
		String id = ctx.pathParam("id");
		Ciclista ciclista = CiclistaService.getById(id);

		if (ciclista != null) {
			Ciclista temp = ciclistas.stream().filter(c -> id.equals(c.getId())).findAny().orElse(null);

			if (temp != null) {
				ctx.status(200);
				ctx.result(status("200", dadosCadastrados));
			} else {
				ctx.status(404);
				ctx.result(status("404", dadosNaoEcontrados));
			}
		} else {
			ctx.status(422);
			ctx.result(status("422", dadosInvalidos));
		}

	}

	public static void putCiclistaById(Context ctx) {
		String id = ctx.pathParam("id");
		String body = ctx.body();
		Ciclista ciclista = CiclistaService.getById(id);

		if (ciclista != null) {
			Ciclista temp = ciclistas.stream().filter(c -> id.equals(c.getId())).findAny().orElse(null);

			if (temp != null) {
				CiclistaService.putCiclistaById(id, body);
				ctx.status(200);
				ctx.result(status("200", dadosCadastrados));
			} else {
				ctx.status(404);
				ctx.result(status("404", dadosNaoEcontrados));
			}
		} else {
			ctx.status(405);
			ctx.result(status("405", dadosInvalidos));
		}

	}

	public static void postCiclistaAtivar(Context ctx) {
		String id = ctx.pathParam("id");
		Ciclista ciclista = CiclistaService.getById(id);

		if (ciclista != null) {
			Ciclista temp = ciclistas.stream().filter(c -> id.equals(c.getId())).findAny().orElse(null);

			if (temp != null) {
				ctx.status(200);
				ctx.result(status("200", dadosCadastrados));
			} else {
				ctx.status(404);
				ctx.result(status("404", dadosNaoEcontrados));
			}
		} else {
			ctx.status(405);
			ctx.result(status("405", dadosInvalidos));
		}

	}

	public static void getCiclistaEmail(Context ctx) {
		String mail = ctx.pathParam("email");
		Ciclista ciclista = CiclistaService.getCiclistaEmail(mail);

		if (ciclista != null) {

			if (email.matcher(mail).matches() == false) {
				ctx.status(400);
				ctx.result(status("400", "Email não enviado como parâmetro"));
			}

			Ciclista temp = ciclistas.stream().filter(e -> email.equals(e.getEmail())).findAny().orElse(null);

			if (temp != null) {
				ctx.status(200);
				ctx.result(status("200", "true"));
			} else {
				ctx.status(200);
				ctx.result(status("200", "false"));
			}
		} else {
			ctx.status(422);
			ctx.result(status("422", dadosInvalidos));
		}

	}

	public static Ciclista mockGetCiclista() {
		CiclistaService.ciclistas.clear();
		MeioDePagamento meio1 = new MeioDePagamento("Helena", "12345", "01-01-2130", "123");
		Ciclista ciclista1 = new Ciclista();

		ciclista1.setNome("Helena");
		ciclista1.setNascimento("01-01-2001");
		ciclista1.setCpf("11000111001");
		ciclista1.setPassaporte(new Ciclista.Passaporte());
		ciclista1.getPassaporte().setNumero("123456789");
		ciclista1.getPassaporte().setPais("Brasil");
		ciclista1.getPassaporte().setValidade("01-01-2050");
		ciclista1.setNacionalidade("brasileira");
		ciclista1.setEmail("helena@hotmail.com");
		ciclista1.setSenha("senha1");
		ciclista1.setMeioDePagamento(meio1);

		ciclistas.add(ciclista1);
		CiclistaService.envia(ciclista1);

		return CiclistaService.ciclistas.get(CiclistaService.ciclistas.size() - 1);

	}

	public static Ciclista mockGetCiclista404() {
		CiclistaService.ciclistas.clear();
		MeioDePagamento meio3 = new MeioDePagamento("Joao", "12345", "01-01-3030", "123");
		Ciclista ciclista3 = new Ciclista();

		ciclista3.setNome("Joao");
		ciclista3.setNascimento("01-01-2009");
		ciclista3.setCpf("78945612300");
		ciclista3.setPassaporte(new Ciclista.Passaporte());
		ciclista3.getPassaporte().setNumero("7894561236");
		ciclista3.getPassaporte().setPais("Alemanha");
		ciclista3.getPassaporte().setValidade("03-01-2090");
		ciclista3.setNacionalidade("alemã");
		ciclista3.setEmail("jo@hotmail.com");
		ciclista3.setSenha("senha2");
		ciclista3.setMeioDePagamento(meio3);

		CiclistaService.envia(ciclista3);

		return CiclistaService.ciclistas.get(CiclistaService.ciclistas.size() - 1);

	}

	public static Ciclista mockGetCiclista405() {
		CiclistaService.ciclistas.clear();
		MeioDePagamento meio1 = new MeioDePagamento("Gi", "12345", "01-01-2032", "123");
		Ciclista ciclista1 = new Ciclista();

		ciclista1.setNome("Giovanna");
		ciclista1.setNascimento("01-06-2011");
		ciclista1.setCpf("11111111111");
		ciclista1.setPassaporte(new Ciclista.Passaporte());
		ciclista1.getPassaporte().setNumero("1010101010");
		ciclista1.getPassaporte().setPais("Brasil");
		ciclista1.getPassaporte().setValidade("01-08-2059");
		ciclista1.setNacionalidade("brasileira");
		ciclista1.setEmail("gi@hotmail.com");
		ciclista1.setSenha("senha3");
		ciclista1.setMeioDePagamento(meio1);
		ciclista1.setId("iiii");

		ciclistas.add(ciclista1);
		CiclistaService.envia(ciclista1);

		return CiclistaService.ciclistas.get(CiclistaService.ciclistas.size() - 1);

	}

	public static Ciclista mockGetCiclista422() {
		CiclistaService.ciclistas.clear();
		MeioDePagamento meio4 = new MeioDePagamento("Jose", "12345", "01-01-1999", "123");
		Ciclista ciclista4 = new Ciclista();

		ciclista4.setNome("Jose");
		ciclista4.setNascimento("12-03-1965");
		ciclista4.setCpf("22222222222");
		ciclista4.setPassaporte(new Ciclista.Passaporte());
		ciclista4.getPassaporte().setNumero("789666963");
		ciclista4.getPassaporte().setPais("Suecia");
		ciclista4.getPassaporte().setValidade("12-05-2025");
		ciclista4.setNacionalidade("sueco");
		ciclista4.setEmail("josejjj@hotmail.com");
		ciclista4.setSenha("senha4");
		ciclista4.setMeioDePagamento(meio4);
		ciclista4.setId("aaaaaaaaa");

		ciclistas.add(ciclista4);
		CiclistaService.envia(ciclista4);

		return CiclistaService.ciclistas.get(CiclistaService.ciclistas.size() - 1);

	}

	public static Ciclista mockGetEmail() {
		CiclistaService.ciclistas.clear();
		MeioDePagamento meio5 = new MeioDePagamento("000", "12345", "01-01-2001", "123");
		Ciclista ciclista5 = new Ciclista();

		ciclista5.setNome("Bruna");
		ciclista5.setNascimento("12-12-1967");
		ciclista5.setCpf("3333333333");
		ciclista5.setPassaporte(new Ciclista.Passaporte());
		ciclista5.getPassaporte().setNumero("1110002220");
		ciclista5.getPassaporte().setPais("Coreia do Sul");
		ciclista5.getPassaporte().setValidade("19-05-2625");
		ciclista5.setNacionalidade("coreana");
		ciclista5.setEmail("bruuu@hotmail.com");
		ciclista5.setSenha("bruna123");
		ciclista5.setMeioDePagamento(meio5);

		ciclistas.add(ciclista5);
		CiclistaService.envia(ciclista5);

		return CiclistaService.ciclistas.get(CiclistaService.ciclistas.size() - 1);

	}

	public static Ciclista mockGetEmail400() {
		CiclistaService.ciclistas.clear();
		MeioDePagamento meio6 = new MeioDePagamento("000", "12345", "01-01-2056", "123");
		Ciclista ciclista6 = new Ciclista();

		ciclista6.setNome("Julia");
		ciclista6.setNascimento("23-01-1987");
		ciclista6.setCpf("5555555555");
		ciclista6.setPassaporte(new Ciclista.Passaporte());
		ciclista6.getPassaporte().setNumero("11005544778");
		ciclista6.getPassaporte().setPais("Argentina");
		ciclista6.getPassaporte().setValidade("12-02-2023");
		ciclista6.setNacionalidade("argentina");
		ciclista6.setEmail("");
		ciclista6.setSenha("juuuhhh");
		ciclista6.setMeioDePagamento(meio6);

		ciclistas.add(ciclista6);
		CiclistaService.envia(ciclista6);

		return CiclistaService.ciclistas.get(CiclistaService.ciclistas.size() - 1);

	}


	public static class PostCiclista {
		@JsonProperty
		CiclistaNovo ciclista;
		@JsonProperty
		MeioDePagamentoNovo meioDePagamento;
	}

	public static class CiclistaNovo {
		@JsonProperty
		public String id;
		public String nome;
		public String nascimento;
		public String cpf;
		public String nacionalidade;
		public String email;
		public String senha;
		@JsonProperty
		public PassaporteNovo passaporte;

		public CiclistaNovo() {

		}

		public CiclistaNovo(Ciclista ciclista) {
			if (ciclista.getId() != null) {
				this.id = ciclista.getId().toString();
			}
			this.nome = ciclista.getNome();
			this.nascimento = ciclista.getNascimento().toString();
			this.cpf = ciclista.getCpf();
			this.nacionalidade = ciclista.getNacionalidade();
			this.email = ciclista.getEmail();
			this.senha = ciclista.getSenha();
			if (ciclista.getPassaporte() != null) {
				this.passaporte = new PassaporteNovo(ciclista.getPassaporte());
			}
		}

		public static class PassaporteNovo {
			@JsonProperty
			public String numero, validade, pais;

			public PassaporteNovo() {

			}

			public PassaporteNovo(Passaporte passaporte) {
				this.numero = passaporte.getNumero();
				this.validade = passaporte.getValidade().toString();
				this.pais = passaporte.getPais();
			}

			public Passaporte obterPassaporte() {
				Passaporte passaporte = new Passaporte();

				String validade = this.validade;

				passaporte.setNumero(this.numero);
				passaporte.setValidade(validade);
				passaporte.setPais(this.pais);

				return passaporte;
			}
		}

		public Ciclista obterCiclista() {
			Ciclista ciclista = new Ciclista();

			String nascimento = this.nascimento;

			if (this.id != null) {
				ciclista.setId((this.id));
			}
			ciclista.setNome(this.nome);
			ciclista.setNascimento(nascimento);
			ciclista.setCpf(this.cpf);
			ciclista.setNacionalidade(this.nacionalidade);
			ciclista.setEmail(this.email);
			ciclista.setSenha(this.senha);
			if (this.passaporte != null) {
				ciclista.setPassaporte(this.passaporte.obterPassaporte());
			}

			return ciclista;
		}
	}

	public static class MeioDePagamentoNovo {
		@JsonProperty
		String nomeTitular;
		String numero;
		String validade;
		String cvv;

		public String getNomeTitular() {
			return nomeTitular;
		}

		public void setNomeTitular(String nomeTitular) {
			this.nomeTitular = nomeTitular;
		}

		public String getNumero() {
			return numero;
		}

		public void setNumero(String numero) {
			this.numero = numero;
		}

		public String getValidade() {
			return validade;
		}

		public void setValidade(String validade) {
			this.validade = validade;
		}

		public String getCvv() {
			return cvv;
		}

		public void setCvv(String cvv) {
			this.cvv = cvv;
		}
	}

}